<?php

/**
 * Entity storage interface with EFQ support
 */
interface QueryableFieldStorageInterface extends FieldStorageInterface
{
    /**
     * Implements hook_field_storage_query()
     */
    public function query(EntityFieldQuery $query);
}
