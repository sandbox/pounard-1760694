<?php

/**
 * Entity storage interface with no EFQ support
 */
interface FieldStorageInterface
{
    /**
     * Implements hook_field_storage_info()
     */
    static public function getInfo();

    /**
     * Implements hook_field_storage_create_field()
     */
    public function createField($field);

    /**
     * Implements hook_field_update_forbid()
     */
    public function updateFieldForbid($field, $priorField, $hasData);

    /**
     * Implements hook_field_storage_update_field()
     */
    public function updateField($field, $priorField, $hasData);

    /**
     * Implements hook_field_storage_load()
     */
    public function loadEntities($entityType, $entities, $age, $fields, $options);

    /**
     * Implements hook_field_storage_delete_field()
     */
    public function deleteField($field);

    /**
     * Implements hook_field_storage_write()
     */
    public function saveEntity($entityType, $entity, $op, $fields);

    /**
     * Implements hook_field_storage_delete()
     */
    public function deleteEntity($entityType, $entity, $fields);

    /**
     * Implements hook_field_storage_purge()
     */
    public function purgeEntity($entityType, $entity, $field, $instance);

    /**
     * Implements hook_field_storage_delete_revision()
     */
    public function deleteEntityRevision($entityType, $entity, $fields);

    /**
     * Implements hook_field_storage_delete_instance()
     */
    public function deleteInstance($instance);

    /**
     * Implements hook_field_attach_rename_bundle()
     */
    public function renameBundle($entityType, $bundleOld, $bundleNew);

    /**
     * Implements hook_field_storage_purge_field()
     */
    public function purgeField($field);
}
