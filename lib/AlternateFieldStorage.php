<?php

class AlternateFieldStorage implements QueryableFieldStorageInterface
{
    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::getInfo()
     */
    static public function getInfo()
    {
        return array(
            'alternate_field_storage' => array(
                'label' => t('Alternate SQL storage'),
                'description' => t('Stores fields in the local SQL database.'),
            ),
        );
    }

    /**
     * @var array
     */
    protected $crcIndexCache;

    /**
     * Generate a table name for a field data table
     *
     * @param array $field The field structure
     *
     * @return string      A string containing the generated name for the
     *                     database table
     */
    protected function getTableName($field)
    {
        return "field_data_{$field['field_name']}";
    }

    /**
     * Generate a column name for a field data table
     *
     * @param string $column The name of the column
     *
     * @return string        A string containing a generated column name for a
     *                       field data table that is unique among all other
     *                       fields
     */
    protected function getColumnName($column)
    {
        return 'data_' . $column;
    }

    /**
     * Generate an index name for a field data table
     *
     * @param string $index The name of the index
     *
     * @return string       A string containing a generated index name for a
     *                      field data table that is unique among all other
     *                      fields
     */
    protected function getIndexName($index)
    {
        return 'data_' . $index . 'idx';
    }

    /**
     * Compute and store a string CRC32 integer value
     *
     * @param string $string String to hash, that will then be stored into our
     *                       index table
     *
     * @return int|string    CRC32 integer value, casted as string on 32bits
     *                       systems
     */
    protected function getCrc($string)
    {
        if (null === $this->crcIndexCache) {
            $this->crcIndexCache = db_select('field_crc_index', 'i')
                ->fields('i', array('name', 'crc'))
                ->execute()
                ->fetchAllKeyed();
        }

        if (!isset($this->crcIndexCache[$string])) {

            if (PHP_INT_MAX === 2147483647) {
                // 32bits system
                $crc = ___crc32($string);
            } else {
                // 64bits system
                $crc = crc32($string);
            }

            db_insert('field_crc_index')
                ->fields(array('name', 'crc'))
                ->values(array(
                    'name' => $string,
                    'crc'  => $crc,
                ))
                ->execute();

            $this->crcIndexCache[$string] = $crc;
        }

        return $this->crcIndexCache[$string];
    }

    /**
     * Return the database table schema for the given field
     *
     * @param array $field The field structure for which to generate a
     *                     database schema
     *
     * @return array       A table schema array
     */
    public function getTableSchemaForField(array $field)
    {
        $current = array(
            'description' => "Data storage for field ({$field['field_name']})",
            'fields' => array(
                // FIXME: Is bundle information really necessary here?
                'type_crc' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => true,
                ),
                'bundle_crc' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => true,
                ),
                'lang_crc' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => true,
                ),
                'deleted' => array(
                    'type' => 'int',
                    'size' => 'tiny',
                    'not null' => true,
                    'default' => 0,
                    'description' => 'A boolean indicating whether this data item has been deleted',
                ),
                'entity_id' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => true,
                    'description' => 'The entity id this data is attached to',
                ),
                'revision_id' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => false,
                    'description' => 'The entity revision id this data is attached to, or null if the entity type is not versioned',
                ),
                'delta' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => true,
                    'description' => 'The sequence number for this data item, used for multi-value fields',
                ),
            ),
            'primary key' => array('type_crc', 'lang_crc', 'entity_id', 'revision_id', 'delta'),
            'indexes' => array(
                 // See later for indexes.
                 'type_id_lang_idx' => array('type_crc', 'entity_id', 'lang_crc'),
                 'bundle_crc_idx' => array('bundle_crc'),
            ),
        );

        $field += array(
            'columns' => array(),
            'indexes' => array(),
            'foreign keys' => array(),
        );
        // Add field columns
        foreach ($field['columns'] as $columnName => $attributes) {
            $current['fields'][$this->getColumnName($columnName)] = $attributes;
        }

        // Add indexes
        foreach ($field['indexes'] as $indexName => $columns) {
            $realName = $this->getIndexName($indexName);
            foreach ($columns as $columnName) {
                $current['indexes'][$realName][] = $this->getColumnName($columnName);
            }
        }

        // Add foreign keys
        foreach ($field['foreign keys'] as $specifier => $specification) {
            $realName = $this->getIndexName($specifier);
            $current['foreign keys'][$realName]['table'] = $specification['table'];
            foreach ($specification['columns'] as $columnName => $referenced) {
                $realColumnName = $this->getColumnName($columnName);
                $current['foreign keys'][$realName]['columns'][$realColumnName] = $referenced;
            }
        }

        return array($this->getTableName($field) => $current);
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::createField()
     */
    public function createField($field)
    {
        foreach ($this->getTableSchemaForField($field) as $name => $table) {
            db_create_table($name, $table);
        }

        // Ensure schema cache is updated accordingly
        drupal_get_schema(null, true);
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::fieldUpdateForbid()
     */
    public function updateFieldForbid($field, $priorField, $hasData)
    {
        if ($hasData && ($field['columns'] != $priorField['columns'])) {
            throw new FieldUpdateForbiddenException(
                "alternate_field_storage cannot change the schema for an existing field with data.");
        }
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::updateField()
     */
    public function updateField($field, $priorField, $hasData)
    {
  throw new FieldUpdateForbiddenException("alternate_field_storage cannot change the schema for an existing field with data.");
  /*
   * FIXME: Implement this
   *
  if (! $has_data) {
    // There is no data. Re-create the tables completely.

    if (Database::getConnection()->supportsTransactionalDDL()) {
      // If the database supports transactional DDL, we can go ahead and rely
      // on it. If not, we will have to rollback manually if something fails.
      $transaction = db_transaction();
    }

    try {
      $prior_schema = $this->getTableSchemaForField($prior_field);
      foreach ($prior_schema as $name => $table) {
        db_drop_table($name, $table);
      }
      $schema = $this->getTableSchemaForField($field);
      foreach ($schema as $name => $table) {
        db_create_table($name, $table);
      }
    }
    catch (Exception $e) {
      if (Database::getConnection()->supportsTransactionalDDL()) {
        $transaction->rollback();
      }
      else {
        // Recreate tables.
        $prior_schema = $this->getTableSchemaForField($prior_field);
        foreach ($prior_schema as $name => $table) {
          if (!db_table_exists($name)) {
            db_create_table($name, $table);
          }
        }
      }
      throw $e;
    }
  }
  else {
    // There is data, so there are no column changes. Drop all the
    // prior indexes and create all the new ones, except for all the
    // priors that exist unchanged.
    $table = _alternate_field_storage_tablename($prior_field);
    foreach ($prior_field['indexes'] as $name => $columns) {
      if (!isset($field['indexes'][$name]) || $columns != $field['indexes'][$name]) {
        $real_name = _alternate_field_storage_indexname($name);
        db_drop_index($table, $real_name);
      }
    }
    $table = _alternate_field_storage_tablename($field);
    foreach ($field['indexes'] as $name => $columns) {
      if (!isset($prior_field['indexes'][$name]) || $columns != $prior_field['indexes'][$name]) {
        $real_name = _alternate_field_storage_indexname($name);
        $real_columns = array();
        foreach ($columns as $column_name) {
          $real_columns[] = $this->getColumnName($column_name);
        }
        db_add_index($table, $real_name, $real_columns);
      }
    }
  }
  drupal_get_schema(null, true);
  */
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::load()
     */
    function loadEntities($entityType, $entities, $age, $fields, $options)
    {
        $fieldInfo   = field_info_field_by_ids();
        $loadCurrent = $age == FIELD_LOAD_CURRENT;
        $typeCrc     = $this->getCrc($entityType);

        foreach ($fields as $fieldId => $ids) {

            $field        = $fieldInfo[$fieldId];
            $fieldName    = $field['field_name'];
            $table        = $this->getTableName($field);
            $crcLanguages = array();

            foreach (field_available_languages($entityType, $field) as $langcode) {
                $crc                = $this->getCrc($langcode);
                $crcLanguages[$crc] = $langcode;
            }

            $query = db_select($table, 't')
                ->fields('t')
                // This won't work if we have more than once bundle
                ->condition('type_crc', $typeCrc)
                ->condition($loadCurrent ? 'entity_id' : 'revision_id', $ids, 'IN')
                ->condition('lang_crc', array_keys($crcLanguages), 'IN')
                ->orderBy('delta');

            if (empty($options['deleted'])) {
                $query->condition('deleted', 0);
            }

            $deltaCount = array();
            foreach ($query->execute() as $row) {

                $langcode = $crcLanguages[$row->lang_crc];

                if (!isset($deltaCount[$row->entity_id][$langcode])) {
                    $deltaCount[$row->entity_id][$langcode] = 0;
                }

                if ($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED ||
                    $deltaCount[$row->entity_id][$langcode] < $field['cardinality'])
                {
                    $item = array();
                    // For each column declared by the field, populate the item
                    // from the prefixed database column.
                    foreach ($field['columns'] as $column => $attributes) {
                        $column_name = $this->getColumnName($column);
                        $item[$column] = $row->$column_name;
                    }

                    // Add the item to the field values for the entity.
                    $entities[$row->entity_id]->{$fieldName}[$langcode][] = $item;
                    $deltaCount[$row->entity_id][$langcode]++;
                }
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::deleteField()
     */
    public function deleteField($field)
    {
        db_update($this->getTableName($field))
            ->fields(array('deleted' => 1))
            ->execute();
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::saveEntity()
     */
    function saveEntity($entityType, $entity, $op, $fields)
    {
        list($id, $vid, $bundle) = entity_extract_ids($entityType, $entity);

        if (!isset($vid)) {
            $vid = $id;
        }

        $typeCrc   = $this->getCrc($entityType);
        $bundleCrc = $this->getCrc($bundle);

        foreach ($fields as $fieldId) {

            $field          = field_info_field_by_id($fieldId);
            $fieldName      = $field['field_name'];
            $tableName      = $this->getTableName($field);
            $allLanguages   = field_available_languages($entityType, $field);
            $fieldLanguages = array_intersect($allLanguages, array_keys((array)$entity->$fieldName));

            // Delete and insert, rather than update, in case a value was added
            if ($op == FIELD_STORAGE_UPDATE) {
                // Delete languages present in the incoming $entity->$fieldName
                // Delete all languages if $entity->$fieldName is empty
                $languages = !empty($entity->$fieldName) ? $fieldLanguages : $allLanguages;

                $languagesCrc = array();
                foreach ($languages as $langcode) {
                    $languagesCrc[] = $this->getCrc($langcode);
                }

                if ($languages) {
                    db_delete($tableName)
                        ->condition('type_crc', $typeCrc)
                        ->condition('entity_id', $id)
                        ->condition('lang_crc', $languagesCrc, 'IN')
                        ->execute();
                }
            }

            $doInsert = false;

            $columns = array('type_crc', 'bundle_crc', 'entity_id', 'revision_id', 'delta', 'lang_crc');
            foreach ($field['columns'] as $column => $attributes) {
                $columns[] = $this->getColumnName($column);
            }

            $query = db_insert($tableName)->fields($columns);

            foreach ($fieldLanguages as $langcode) {

                $langCrc    = $this->getCrc($langcode);
                $items      = (array) $entity->{$fieldName}[$langcode];
                $deltaCount = 0;

                foreach ($items as $delta => $item) {

                    $doInsert = true;

                    $record = array(
                        'type_crc'    => $typeCrc,
                        'bundle_crc'  => $bundleCrc,
                        'entity_id'   => $id,
                        'revision_id' => $vid,
                        'delta'       => $delta,
                        'lang_crc'    => $langCrc,
                    );

                    foreach ($field['columns'] as $column => $attributes) {
                        if (isset($item[$column])) {
                            $record[$this->getColumnName($column)] = $item[$column];
                        } else {
                            $record[$this->getColumnName($column)] = null;
                        }
                    }

                    $query->values($record);

                    if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED &&
                        ++$deltaCount == $field['cardinality'])
                    {
                        break;
                    }
                }
            }

            if ($doInsert) {
                $query->execute();
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::deleteEntity()
     */
    public function deleteEntity($entityType, $entity, $fields)
    {
        list($id, $vid, $bundle) = entity_extract_ids($entityType, $entity);

        foreach (field_info_instances($entityType, $bundle) as $instance) {
            if (isset($fields[$instance['field_id']])) {
                $field = field_info_field_by_id($instance['field_id']);
                $this->purgeEntity($entityType, $entity, $field, $instance);
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::purgeEntity()
     */
    public function purgeEntity($entityType, $entity, $field, $instance)
    {
        list($id, $vid, $bundle) = entity_extract_ids($entityType, $entity);

        db_delete($this->getTableName($field))->condition('entity_id', $id)->execute();
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::deleteEntityRevision()
     */
    public function deleteEntityRevision($entityType, $entity, $fields)
    {
        list($id, $vid, $bundle) = entity_extract_ids($entityType, $entity);

        if (!isset($vid)) {
            return;
        }

        $typeCrc = $this->getCrc($entityType);

        foreach ($fields as $fieldId) {
            $field = field_info_field_by_id($fieldId);

            db_delete($this->getTableName($field))
                ->condition('type_crc', $typeCrc)
                ->condition('entity_id', $id)
                ->condition('revision_id', $vid)
                ->execute();
        }
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::deleteInstance()
     */
    public function deleteInstance($instance)
    {
        $typeCrc   = $this->getCrc($instance['entity_type']);
        $bundleCrc = $this->getCrc($instance['bundle']);
        $field     = field_info_field($instance['field_name']);

        db_update($this->getTableName($field))
            ->fields(array('deleted' => 1))
            ->condition('type_crc', $typeCrc)
            ->condition('bundle_crc', $bundleCrc)
            ->execute();
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::renameBundle()
     */
    public function renameBundle($entityType, $bundleOld, $bundleNew)
    {
        throw new Exception("Not implemented yet");

        /*
         // We need to account for deleted or inactive fields and instances.
        $instances = field_read_instances(array(
            'entity_type' => $entityType,
            'bundle'      => $bundleNew,
        ), array(
            'include_deleted'  => true,
            'include_inactive' => true,
        ));

        foreach ($instances as $instance) {
            $field = field_info_field_by_id($instance['field_id']);

            if ($field['storage']['type'] == 'alternate_field_storage') {
                db_update($this->getTableName($field))
                    ->fields(array('bundle' => $bundleNew))
                    ->condition('entity_type', $entityType)
                    ->condition('bundle', $bundleOld)
                    ->execute();
            }
        }
        */
    }

    /**
     * (non-PHPdoc)
     * @see FieldStorageInterface::purgeField()
     */
    public function purgeField($field)
    {
        db_drop_table($this->getTableName($field));
    }

    /**
     * (non-PHPdoc)
     * @see QueryableFieldStorageInterface::query()
     */
    public function query(EntityFieldQuery $query)
    {
        if ($query->age == FIELD_LOAD_CURRENT) {
            $idKey = 'entity_id';
        } else {
            $idKey = 'revision_id';
        }

        $aliases   = array();
        $select    = null;
        $baseTable = null;

        /**********************************************************************
         * First JOIN all field tables
         **********************************************************************/

        // Add tables for the fields used
        foreach ($query->fields as $key => $field) {

            // Every field needs a new table
            $table         = $this->getTableName($field);
            $alias         = $table . $key;
            $aliases[$key] = $alias;

            if (!isset($select)) {
                $baseTable = $alias;

                // First table will be our main query table, others will JOIN
                // seamlessly into it
                $select = db_select($table, $alias);
                $select->addMetaData('base_table', $table);

                // We need to join with the CRC32 index table to ensure we fetch
                // the right identifiers, this is the main downside of this
                // storage engine
                // Because those index tables will remain very small, we
                // shouldn't have a huge impact on queries
                $select->join('field_crc_index', 'ie', "ie.crc = $alias.type_crc");
                $select->join('field_crc_index', 'ib', "ib.crc = $alias.bundle_crc");

                // Necessary result fields for classical field queries.
                $select->addField($alias, 'entity_id');
                $select->addField($alias, 'revision_id');
                $select->addField('ie', 'name', 'entity_type');
                $select->addField('ib', 'name', 'bundle');

                // Allow queries internal to the Field API to opt out of the
                // access check, for situations where the query's results should
                // not depend on the access grants for the current user
                if (!isset($query->tags['DANGEROUS_ACCESS_CHECK_OPT_OUT'])) {
                    $select->addTag('entity_field_access');
                }
            } else {
                // This is not the first table, we need to JOIN with the
                // arbitrarily choosen first field table
                $join = array(
                    "$alias.type_crc = $baseTable.type_crc",
                    "$alias.$idKey = $baseTable.$idKey",
                );

                $select->join($table, $alias, implode(" AND ", $join));
            }

            // We don't care about cardinality here, let's just ensure we will
            // never get duplicate identifiers in the result
            $select->groupBy("$baseTable.entity_id");
        }

        /**********************************************************************
         * Deal with field conditions
         **********************************************************************/

        $groups  = array();
        $metaMap = array(
            'delta'    => 'delta',
            'language' => 'lang_crc',
        );

        // Add field conditions
        foreach ($query->fieldConditions as $key => $condition) {
            $alias    = $aliases[$key];
            $sqlfield = "$alias." . $this->getColumnName($condition['column']);
            $query->addCondition($select, $sqlfield, $condition);
            // Add delta / language group conditions // WHAT!!!
            foreach ($metaMap as $type => $column) {
                if (isset($condition[$type . '_group'])) {
                    $groupName = $condition[$type . '_group'];
                    if (!isset($groups[$type][$groupName])) {
                        $groups[$type][$groupName] = $alias;
                    } else {
                        $select->where("$alias.$column = " . $groups[$type][$groupName] . ".$column");
                    }
                }
            }
        }

        // Add field meta conditions
        foreach ($query->fieldMetaConditions as $key => $condition) {
            $alias    = $aliases[$key];
            $sqlfield = "$alias." . $condition['column'];
            $query->addCondition($select, $sqlfield, $condition);
            // Add delta / language group conditions // WHAT!!!
            foreach ($metaMap as $type => $column) {
                if (isset($condition[$type . '_group'])) {
                    $groupName = $condition[$type . '_group'];
                    if (!isset($groups[$type][$groupName])) {
                        $groups[$type][$groupName] = $alias;
                    } else {
                        $select->where("$alias.$column = " . $groups[$type][$groupName] . ".$column");
                    }
                }
            }
        }

        /**********************************************************************
         * Allow deleted items to be retrieved
         **********************************************************************/

        if (isset($query->deleted)) {
            $select->condition("$baseTable.deleted", (int)$query->deleted);
        }

        /**********************************************************************
         * Deal with ordering
         **********************************************************************/

        // Is there a need to sort the query by property?
        $hasOrder = false;
        foreach ($query->order as $order) {
            if ($order['type'] == 'property') {
                $hasOrder = true;
            }
        }

        /**********************************************************************
         * And with property conditions too
         **********************************************************************/

        if ($query->propertyConditions || $hasOrder) {

            if (empty($query->entityConditions['entity_type']['value'])) {
                throw new EntityFieldQueryException(
                    "Property conditions and orders must have an entity type defined.");
            }

            $entityType      = $query->entityConditions['entity_type']['value'];
            $entityInfo      = entity_get_info($entityType);
            $entityBaseTable = $entityInfo['base table'];
            $entityField     = $entityInfo['entity keys']['id'];

            $select->join(
                $entityBaseTable,
                $entityBaseTable,
                "$entityBaseTable.$entityField = $baseTable.entity_id");

            $query->entityConditions['entity_type']['operator'] = '=';

            foreach ($query->propertyConditions as $propertyCondition) {
                $query->addCondition(
                    $select,
                    "$entityBaseTable." . $propertyCondition['column'],
                    $propertyCondition);
            }
        }

        /**********************************************************************
         * And with entity specific conditions (node.changed for example)
         **********************************************************************/

        foreach ($query->entityConditions as $key => $condition) {
            $query->addCondition($select, "$baseTable.$key", $condition);
        }

        /**********************************************************************
         * Finish with the real ordering algorithm
         **********************************************************************/

        // Order the query
        foreach ($query->order as $order) {
            if ($order['type'] == 'entity') {
                $key = $order['specifier'];

                $select->orderBy("$baseTable.$key", $order['direction']);

            } else if ($order['type'] == 'field') {
                $specifier = $order['specifier'];
                $field     = $specifier['field'];
                $alias     = $aliases[$specifier['index']];
                $sqlField = "$alias." . $this->getColumnName($specifier['column']);

                $select->orderBy($sqlField, $order['direction']);

            } else if ($order['type'] == 'property') {
                $select->orderBy("$entityBaseTable." . $order['specifier'], $order['direction']);
            }
        }

        /**********************************************************************
         * Prey
         **********************************************************************/

        return $query->finishQuery($select, $idKey);
    }
}
