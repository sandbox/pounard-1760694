Alternate field storage
=======================

New implemention of 'field_sql_storage' core module:

  * Uses CRC32 computed integers instead of varchar indexes in all field data
    tables, which provides a significant performance boost for index operations
    (join, insert and update operations).

  * Maintain an arbitrary CRC32 index table, in case of conflicts some CRC32
    can be changed. Conflicts are unlikely to happen because all identifiers
    are small enough to avoid that, but for consistency we need a backup plan.

  * Slightly modified schema of data table, with less indexes and columns. This
    reduces drastically storage space taken by those tables, and ensure smaller
    indexes into memory.

  * Removed useless revision table, thus avoiding systematic double insert,
    double delete and double update operations.

  * EFQ compatible.

TODO
====

  * This modules is a raw port of 'field_sql_storage', most of its internals
    must be rewrote and optimized.

  * Fix EFQ filtering on language, bundle and entity_type conditions.

  * The CRC32 index conflict backup plan is not implemented.

  * Test and measure index usage, and do a fine tuning over them in the fields
    data table schema.

  * alternate_field_storage_field_attach_rename_bundle().

  * alternate_field_storage_field_storage_update_field().

  * Testing!
